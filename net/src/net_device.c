/**
	************************************************************
	************************************************************
	************************************************************
	*	文件名： 	NB_DEVICE.c
	*
	*	说明： 		网络设备应用层
	*
	*	修改记录：
	************************************************************
	************************************************************
	************************************************************
**/

//单片机头文件
#include "stm32f10x.h"

//单片机相关组件
#include "mcu_gpio.h"

//网络设备
#include "net_device.h"
#include "net_io.h"
#include "net_define.h"

//硬件驱动
#include "delay.h"
#include "usart.h"
#include "typeconverter.h"
#if(NET_TIME_EN == 1)
#include <time.h>
#endif

//C库
#include <string.h>
#include <stdlib.h>
#include <stdio.h>


static _Bool nd_busy[2] = {0, 0};

NB_DEVICE_INFO NB_DEVICE_info = {NULL, 0, 0, NULL, NULL, 0, 0,
									0, 0, 0, 0, 0, 0, 1, 0};
                                    
NB_DEVICE_INIT nbDeviceInit = {2000, 10, 10, 60, 40};

const static GPIO_LIST NB_DEVICE_gpio_list[] = {
                {NET_RESET_IO_GROUP, NET_RESET_IO_PIN, NET_RESET_IO_NAME},
                {NET_WAKEUP_IO_GROUP, NET_WAKEUP_IO_PIN, NET_WAKEUP_IO_NAME}
			 };

/*
************************************************************
*	函数名称：	NB_DEVICE_IsBusReady
*
*	函数功能：	查询总线是否就绪
*
*	入口参数：	type：1-数据发送		0-命令发送
*
*	返回参数：	0-就绪	1-未就绪
*
*	说明：		
************************************************************
*/
__inline static _Bool NB_DEVICE_IsBusReady(_Bool type){
	_Bool result = 1;
	RTOS_ENTER_CRITICAL();
	if(nd_busy[type] == 0){
		nd_busy[type] = 1;
		result = 0;
	}
	RTOS_EXIT_CRITICAL();
	return result;
}

/*
************************************************************
*	函数名称：	NB_DEVICE_FreeBus
*
*	函数功能：	释放总线
*
*	入口参数：	type：1-数据发送		0-命令发送
*
*	返回参数：	无
*
*	说明：		
************************************************************
*/
__inline static void NB_DEVICE_FreeBus(_Bool type){
	RTOS_ENTER_CRITICAL();
	nd_busy[type] = 0;
	RTOS_EXIT_CRITICAL();
}

/*
************************************************************
*	函数名称：	NB_DEVICE_Delay
*
*	函数功能：	延时
*
*	入口参数：	time：延时时间，毫秒
*
*	返回参数：	无
*
*	说明：		基于当前延时时基
************************************************************
*/
__inline static void NB_DEVICE_Delay(unsigned int time){
	RTOS_TimeDly(time);
}


/*//==========================================================
//	函数名称：	NB_DEVICE_IO_Init
//
//	函数功能：	初始化网络设备IO层
//
//	入口参数：	无
//
//	返回参数：	无
//
//	说明：		初始化网络设备的控制引脚、数据收发功能等
//==========================================================*/
void NB_DEVICE_IO_Init(void){
  int i;
	for(i=0; i < 2; i++)
		MCU_GPIO_Init(NB_DEVICE_gpio_list[i].gpio_group, NB_DEVICE_gpio_list[i].gpio_pin, GPIO_Mode_Out_PP, GPIO_Speed_50MHz, NB_DEVICE_gpio_list[i].gpio_name);
	MCU_GPIO_Output_Ctl(NB_DEVICE_gpio_list[0].gpio_name, Bit_RESET);
	MCU_GPIO_Output_Ctl(NB_DEVICE_gpio_list[1].gpio_name, Bit_RESET);
	NB_IO_Init();											//网络设备数据IO层初始化
	NB_DEVICE_info.reboot = 0;
}

/*//==========================================================
//	函数名称：	NB_DEVICE_Reset
//
//	函数功能：	网络设备复位
//
//	入口参数：	无
//
//	返回参数：	返回复位结果
//
//	说明：		0-成功		1-失败		
//==========================================================*/
_Bool NB_DEVICE_Reset(void){
	_Bool result = 1;
	UsartPrintf(USART_DEBUG, "[INFO] NB 模块初始化...\n");
	MCU_GPIO_Output_Ctl(NB_DEVICE_gpio_list[0].gpio_name, 1);		//复位
	NB_DEVICE_Delay(100);
	MCU_GPIO_Output_Ctl(NB_DEVICE_gpio_list[0].gpio_name, 0);		//结束复位
	NB_DEVICE_Delay(1000);
	UsartPrintf(USART_DEBUG, "[INFO] NB 模块复位\n");
	result = 0;
	return result;
}

/*//==========================================================
//	函数名称：	NB_DEVICE_GetIMEI
//
//	函数功能：	获取IMEI
//
//	入口参数：	无
//
//	返回参数：	0-成功	1-失败
//
//	说明：		国际移动设备识别码(IMEI)
//==========================================================*/
_Bool NB_DEVICE_GetIMEI(void){
	_Bool result = 1;
	char *data_ptr = NULL;
	unsigned char i = 3;
  while(i--) {
		if(!NB_DEVICE_SendCmd(NET_AT_CMD_CGSN, "+CGSN", 2000)){  // +CGSN: 869060032943213
			data_ptr = strstr(NB_DEVICE_info.resp, " ");
			if(data_ptr){
				memcpy(NB_DEVICE_info.imei, ++data_ptr, 15);
				result = 0;
			}
			break;
		}
		NB_DEVICE_Delay(100);
	}
	return result;
}

/*//==========================================================
//	函数名称：	NB_DEVICE_GetIMSI
//
//	函数功能：	获取一个唯一串号
//
//	入口参数：	无
//
//	返回参数：	0-成功	1-失败
//
//	说明：		国际移动用户识别码(IMSI)
//==========================================================*/
_Bool NB_DEVICE_GetIMSI(void){
	_Bool result = 1;
	char *data_ptr = NULL;
	unsigned char i = 3;
	while(i--) {
		if(!NB_DEVICE_SendCmd(NET_AT_CMD_CIMI, "OK", 2000)){  // +CIMI: 460041914013481
			data_ptr = NB_DEVICE_info.resp;
			if(data_ptr){
				memcpy(NB_DEVICE_info.imsi, data_ptr + 2, 15);
				result = 0;
			}
			break;
		}
		NB_DEVICE_Delay(100);
	}
	return result;
}


/*//==========================================================
//	函数名称：	NB_DEVICE_Init
//
//	函数功能：	网络设备初始化
//
//	入口参数：	无
//
//	返回参数：	返回初始化结果
//
//	说明：		0-成功		1-失败
//==========================================================*/
_Bool NB_DEVICE_Init(void){
	_Bool status = 1;
	int i = 0;
	NB_DEVICE_info.net_work = 0;
	switch(NB_DEVICE_info.init_step){
		case 0:
			if(!NB_DEVICE_Reset()){
				for(i=0; i < nbDeviceInit.uart_test; i++){
					if(!NB_DEVICE_SendCmd(NET_AT_CMD_AT, "OK", 1000)){
						UsartPrintf(USART_DEBUG, "[INFO] NB模块重启成功\r\n");
						NB_DEVICE_info.init_step++;
						break;
					}
					NB_DEVICE_Delay(500);
				}
			}
			break;
		case 1:
			NB_DEVICE_Receive(NULL, 50); // 释放前方缓存
			if(!NB_DEVICE_SendCmd(NET_AT_CMD_ATE0, "OK", 1000)){
				UsartPrintf(USART_DEBUG, "[INFO] 关闭回显\r\n");
				NB_DEVICE_info.init_step++;
			}
			break;
		case 2:
			NB_DEVICE_Receive(NULL, 50); // 释放前方缓存
			for(i=0; i < nbDeviceInit.pin_test; i++) {
				if(!NB_DEVICE_SendCmd(NET_AT_CMD_CPIN, "READY", 1000)){
					UsartPrintf(USART_DEBUG, "[INFO] SIM卡状态正常\r\n");
					NB_DEVICE_info.init_step++;
					break;
				}
				NB_DEVICE_Delay(1000);
			}
			if(i >= nbDeviceInit.pin_test)
				NB_DEVICE_info.init_step = 0;
			break;
		case 3:
			NB_DEVICE_Receive(NULL, 50); // 释放前方缓存
			if(!NB_DEVICE_GetIMEI()) {
				UsartPrintf(USART_DEBUG, "[INFO] 国际移动设备识别码(IMEI): %s\n", NB_DEVICE_info.imei);
				NB_DEVICE_info.init_step++;
			}
			break;
		case 4:
			NB_DEVICE_Receive(NULL, 50); // 释放前方缓存
			if(!NB_DEVICE_GetIMSI()) {
				UsartPrintf(USART_DEBUG, "[INFO] 国际移动用户识别码(IMSI): %s\n", NB_DEVICE_info.imsi);
				NB_DEVICE_info.init_step++;
			}
			break;
		case 5:
			NB_DEVICE_Receive(NULL, 10); // 释放前方缓存
			if(!NB_DEVICE_SendCmd(NET_AT_CMD_CPSMS, "OK", 1000)){
				UsartPrintf(USART_DEBUG, "[INFO] 禁用PSM\r\n");
				NB_DEVICE_info.init_step++;
			}
			break;
		case 6:// 读取ICCID BC26
			NB_DEVICE_Receive(NULL, 10); // 释放前方缓存
			if(!NB_DEVICE_SendCmd(NET_AT_CMD_QCCID, "OK", 1000)) {
				unsigned char qccid[21];
				sscanf(NB_DEVICE_info.resp, "%*s%[^\r]", qccid);
				UsartPrintf(USART_DEBUG, "[INFO] 集成电路卡识别码(ICCID)：%s\r\n", qccid);
				NB_DEVICE_info.init_step++;
			}
			break;
		case 7:
			for(i=0; i < nbDeviceInit.signal_test; i++) {
				if(!NB_DEVICE_SendCmd(NET_AT_CMD_CSQ, "OK", 1000)){ // "OK"
					char signal[2];
					int isignal = 0;
					sscanf((char *)NB_DEVICE_info.resp, "%*s%[^,]", signal);
					sscanf((char *)NB_DEVICE_info.resp, "%*s%[^,]", signal);
					isignal = atoi(signal);
					if(isignal >= 12 && isignal <=31){
						UsartPrintf(USART_DEBUG, "[INFO] 信号强度正常(%d)\r\n", isignal);
						NB_DEVICE_info.init_step++;
						break;
					} else
						UsartPrintf(USART_DEBUG, "[WARNING] 信号强度异常(%d)\r\n", isignal);
				}
				NB_DEVICE_Delay(1000);
			}
			if(i >= nbDeviceInit.signal_test)
				NB_DEVICE_info.init_step = 0;
			break;
		case 8:
			for(i=0; i < nbDeviceInit.net_test; i++) {
				if(!NB_DEVICE_SendCmd(NET_AT_CMD_CEREG, "OK", 1000)){
					char reg[1];
					int ireg = 0;
					sscanf((char *)NB_DEVICE_info.resp, "%*[^,],%[^\r]", reg);
					ireg = atoi(reg);
					if(ireg == 1 || ireg == 5){
						UsartPrintf(USART_DEBUG, "[INFO] ESP网络注册成功(%d)\r\n", ireg);
						NB_DEVICE_info.init_step++;
						break;
					} else
						UsartPrintf(USART_DEBUG, "[WARNING] ESP网络注册尚未成功(%d)\r\n", ireg);
				}
				NB_DEVICE_Delay(1000);
			}
			if(i >= nbDeviceInit.net_test)
				NB_DEVICE_info.init_step = 0;
			break;
		case 9:
			if(!NB_DEVICE_SendCmd(NET_AT_CMD_CGPADDR, "+CGPADDR", 1000)) {
				char addr[64];
				sscanf(NB_DEVICE_info.resp, "%*[^,],%[^\r]", addr);
				UsartPrintf(USART_DEBUG, "[INFO] NB模块IP地址：%s\r\n", addr);
				NB_DEVICE_info.init_step++;
			}
			break;
			
		default:
			status = 0;
			NB_DEVICE_info.net_work = 1;
			break;
	}
	return status;
}

/*//==========================================================
//	函数名称：	NB_DEVICE_Close
//
//	函数功能：	关闭网络连接
//
//	入口参数：	无
//
//	返回参数：	0-成功	1-失败
//
//	说明：		
//==========================================================*/
_Bool NB_DEVICE_Close(void){
	_Bool result = 1;
	UsartPrintf(USART_DEBUG, "[INFO] DISCONNECT\r\n");
	result = NB_DEVICE_SendCmd(NET_AT_CMD_MIPLCLOSE, "OK", 5000);
	NB_DEVICE_Delay(30);
	return result;
}

/*//==========================================================
//	函数名称：	NB_DEVICE_Connect
//
//	函数功能：	重连平台
//
//	入口参数：	type：TCP 或 UDP
//				ip：IP地址缓存指针
//				port：端口缓存指针
//
//	返回参数：	返回连接结果
//
//	说明：		0-成功		1-失败
//==========================================================*/
_Bool NB_DEVICE_Connect(void){
	_Bool status = 1;
	unsigned char step = 0;
	unsigned int  flag = 0;
	unsigned int  mark = 0;
	unsigned char buffer_len = 128;
	char *buffer = (char *)NET_MallocBuffer(buffer_len);
	if(buffer == NULL) {
		UsartPrintf(USART_DEBUG, "[ERROR] 申请不到动态内存(NB_DEVICE_Connect)\r\n");
		NET_FreeBuffer(buffer);
		return 1;
	}
	if(!NB_DEVICE_info.net_work) return 1;
	NB_DEVICE_info.onenet = 0;
	switch(NB_DEVICE_info.conn_step){
		case 0:
			if(!NB_DEVICE_SendCmd(NET_AT_CMD_MIPLCREATE, "OK", 1000)){
				char ref[2]; // 0
				sscanf(NB_DEVICE_info.resp, "%*s%[^\r]", ref);
				NB_DEVICE_info.ref = atoi(ref);
				UsartPrintf(USART_DEBUG, "[INFO] 创建基础通信套件成功，实例标识“%d”\r\n", NB_DEVICE_info.ref );
				NB_DEVICE_info.conn_step++;
			}
			break;
		case 1:
			if(!NB_DEVICE_SendCmd(NET_AT_CMD_MIPLADDOBJ_CH, "OK", 1000)){
				UsartPrintf(USART_DEBUG, "[INFO] 创建(甲烷)对象成功\r\n");
				NB_DEVICE_info.conn_step++;
			}
			break;
		case 2:
			if(!NB_DEVICE_SendCmd(NET_AT_CMD_MIPLADDOBJ_TL2, "OK", 1000)){
				UsartPrintf(USART_DEBUG, "[INFO] 创建(温度，灯2)对象成功\r\n");
				NB_DEVICE_info.conn_step++;
			}
			break;
		case 3:
			if(!NB_DEVICE_SendCmd(NET_AT_CMD_MIPLADDOBJ_H, "OK", 1000)){
				UsartPrintf(USART_DEBUG, "[INFO] 创建(湿度)对象成功\r\n");
				NB_DEVICE_info.conn_step++;
			}
			break;
		case 4:
			if(!NB_DEVICE_SendCmd(NET_AT_CMD_MIPLADDOBJ_L1, "OK", 1000)){
				UsartPrintf(USART_DEBUG, "[INFO] 创建(灯1)对象成功\r\n");
				NB_DEVICE_info.conn_step++;
			}
			break;
		case 5:
			if(!NB_DEVICE_SendCmd(NET_AT_CMD_MIPLADDOBJ_L3, "OK", 1000)){
				UsartPrintf(USART_DEBUG, "[INFO] 创建(灯3)对象成功\r\n");
				NB_DEVICE_info.conn_step++;
			}
			break;
		case 6:
			if(!NB_DEVICE_SendCmd(NET_AT_CMD_MIPLADDOBJ_FMQ, "OK", 1000)){
				UsartPrintf(USART_DEBUG, "[INFO] 创建(蜂鸣器)对象成功\r\n");
				NB_DEVICE_info.conn_step++;
			}
			break;
		case 7:
			if(!NB_DEVICE_SendCmd(NET_AT_CMD_MIPLADDOBJ_FJ, "OK", 1000)){
				UsartPrintf(USART_DEBUG, "[INFO] 创建(风机)对象成功\r\n");
				NB_DEVICE_info.conn_step++;
			}
			break;
		case 8:
			if(!NB_DEVICE_SendCmd(NET_AT_CMD_MIPLOPEN, "OK", 1000)){
				UsartPrintf(USART_DEBUG, "[INFO] 注册到OneNET平台 ...\r\n");
				NB_DEVICE_info.conn_step++;
			}
			break;
		case 9:
			if(!NB_DEVICE_MIPLEvent(0, 6)){
				NB_DEVICE_info.conn_step++;
				UsartPrintf(USART_DEBUG, "[INFO] 注册到OneNET平台成功\r\n");
			} else if(strstr(NB_DEVICE_info.resp, "+MIPLOBSERVE")) {
				NB_DEVICE_info.conn_step++;
				UsartPrintf(USART_DEBUG, "[INFO] 注册到OneNET平台成功\r\n");
			}
			break;
		case 10:
			for(step = 0; step <= 50; step++){
				char ref[1], msgid[12], objectid[5], instanceid[5], resourceid[5];
				unsigned int imsgid, iobjectid;
				if(!NB_DEVICE_Receive("+MIPLOBSERVE", 2000)){		//+MIPLOBSERVE:0,208214,1,3316,0,-1
					UsartPrintf(USART_DEBUG, "[INFO] 收到平台观测指令\r\n");
					sscanf(NB_DEVICE_info.resp, "%*[^:]:%[^,],%[^,],%*[^,],%[^,],%[^,],%[-1-9]", ref, msgid, objectid, instanceid, resourceid);
					imsgid = atoi(msgid);
					iobjectid = atoi(objectid);
					if(iobjectid == 3300) {
						NB_DEVICE_info.msgId.ch4 = imsgid;
					} else if(iobjectid == 3311) {
						NB_DEVICE_info.msgId.templed2 = imsgid;
					} else if(iobjectid == 3306) {
						NB_DEVICE_info.msgId.humi = imsgid;
					} else if(iobjectid == 3201) {
						NB_DEVICE_info.msgId.led1 = imsgid;
					} else if(iobjectid == 3312) {
						NB_DEVICE_info.msgId.led3 = imsgid;
					} else if(iobjectid == 3338) {
						NB_DEVICE_info.msgId.fmq = imsgid;
					} else if(iobjectid == 3340) {
						NB_DEVICE_info.msgId.fj = imsgid;
					}
					if(iobjectid == 3300 || iobjectid == 3311 || iobjectid == 3306 || iobjectid == 3201 || iobjectid == 3312 || iobjectid == 3338 || iobjectid == 3340){
						memset(buffer, 0, buffer_len);	//AT+MIPLOBSERVERSP=0,208214,1
						sprintf(buffer, NET_AT_CMD_MIPLOBSERVERSP, ref, msgid, "1");
#ifdef NB_DEBUG
						UsartPrintf(USART_DEBUG, "[DEBUG] BUFFER: %s\r\n", buffer);
#endif
						if(!NB_DEVICE_SendCmd(buffer, "OK", 1000)) {
							UsartPrintf(USART_DEBUG, "[INFO] 响应平台观测指令成功\r\n");
							flag++;
						} else {
							UsartPrintf(USART_DEBUG, "[ERROR] 响应平台观测指令 失败\r\n");
						}
						if(flag >= 7) {//!!!!!!!!!!!!!!!!!!!!!!!!!对象数量有变化时需要修改
							NB_DEVICE_info.conn_step++;
							break;
						}
					}
				}
				NB_DEVICE_Delay(1000);
			}
			break;
		case 11:
			for(step = 0; step <= 50; step++){
				char ref[1], msgid[12], objectid[5];
				unsigned int iobjectid;
				if(!NB_DEVICE_Receive("+MIPLDISCOVER", 5000)){	//+MIPLDISCOVER:0,11609,3316
					UsartPrintf(USART_DEBUG, "[INFO] 收到平台发现请求\r\n");
					sscanf(NB_DEVICE_info.resp, "%*[^:]:%[^,],%[^,],%[^\r]", ref, msgid, objectid);
					iobjectid = atoi(objectid);
					if(iobjectid == 3300 || iobjectid == 3311 || iobjectid == 3306 || iobjectid == 3201 || iobjectid == 3312 || iobjectid == 3338 || iobjectid == 3340){
						memset(buffer, 0, buffer_len);//AT+MIPLDISCOVERRSP=0,11609,1,9,"5700;5701"
						flag = 0;
						if(iobjectid == 3300) {
							sprintf(buffer, NET_AT_CMD_MIPLDISCOVERRSP1, ref, msgid);
							flag = 0x01;
						} else if(iobjectid == 3311) {
							sprintf(buffer, NET_AT_CMD_MIPLDISCOVERRSP2, ref, msgid);
							flag = 0x01<<1;
						} else if(iobjectid == 3306) {
							sprintf(buffer, NET_AT_CMD_MIPLDISCOVERRSP3, ref, msgid);
							flag = 0x01<<2;
						} else if(iobjectid == 3201) {
							sprintf(buffer, NET_AT_CMD_MIPLDISCOVERRSP4, ref, msgid);
							flag = 0x01<<3;
						} else if(iobjectid == 3312) {
							sprintf(buffer, NET_AT_CMD_MIPLDISCOVERRSP5, ref, msgid);
							flag = 0x01<<4;
						} else if(iobjectid == 3338) {
							sprintf(buffer, NET_AT_CMD_MIPLDISCOVERRSP5, ref, msgid);
							flag = 0x01<<5;
						} else if(iobjectid == 3340) {
							sprintf(buffer, NET_AT_CMD_MIPLDISCOVERRSP5, ref, msgid);
							flag = 0x01<<6;
						}
						if(!NB_DEVICE_SendCmd(buffer, NULL, 5000)){
							if(flag > 0) mark += flag;
							UsartPrintf(USART_DEBUG, "[INFO] 响应平台发现请求成功 %d, %d\r\n", flag, mark);
						} else {
							UsartPrintf(USART_DEBUG, "[ERROR] 响应平台发现请求失败\r\n");
						}
						if(mark==0x7F){
							NB_DEVICE_info.conn_step++;
							break;
						}
					}
				}
				NB_DEVICE_Delay(1000);
			}
			break;
		
		default:
			status = 0;
			NB_DEVICE_info.onenet = 1;
			break;
	}
	NET_FreeBuffer(buffer);
	return status;
}

/*//==========================================================
//	函数名称：	NB_DEVICE_Update
//
//	返回参数：	返回连接结果
//
//	说明：		0-成功		1-失败
//==========================================================*/
_Bool NB_DEVICE_Update(void) {
	unsigned char status = 1;
	if(!NB_DEVICE_SendCmd(NET_AT_CMD_MIPLUPDATE, "OK", 2000)) {
		status = 0;
	}
	return status;
}

/*//==========================================================
//	函数名称：	NB_DEVICE_MIPLEvent
//
//	返回参数：	返回连接结果
//
//	说明：		0-成功		1-失败
//==========================================================*/
_Bool NB_DEVICE_MIPLEvent(unsigned char ref, unsigned char event){
	unsigned char status = 1;
	char ref_str[2];
	unsigned char ref_val = 0;
	char event_str[2];
	unsigned char event_val = 0;
	if(!NB_DEVICE_Receive("+MIPLEVENT", 2000)){	// +MIPLEVENT:0,21
		sscanf(NB_DEVICE_info.resp, "%*[^:]:%[^,],%[^\r]", ref_str, event_str);
#ifdef NB_DEBUG
		UsartPrintf(USART_DEBUG, "[DEBUG] MIPLEVENT:%s, %s\r\n", ref_str, event_str);
#endif
		ref_val = atoi(ref_str);
		event_val = atoi(event_str);
		if(ref_val == ref && event_val == event)
			status = 0;
	}
	return status;
}

/*//==========================================================
//	函数名称：	NB_DEVICE_SendCmd
//
//	函数功能：	向网络设备发送一条命令，并等待正确的响应
//
//	入口参数：	cmd：需要发送的命令
//				res：需要检索的响应
//				timeout(以系统时基为准)
//
//	返回参数：	返回连接结果
//
//	说明：		0-成功		1-失败
//==========================================================*/
_Bool NB_DEVICE_SendCmd(char *cmd, char *expect, unsigned short timeout){
	_Bool result = 1;
	if(NB_DEVICE_IsBusReady(0) == 1) {
		UsartPrintf(USART_DEBUG, "[WARNING] 串口忙，命令发送失败\r\n");
		return 1;
	}
#ifdef NB_DEBUG
	UsartPrintf(USART_DEBUG, "[DEBUG Send] NB Send:%s\n", cmd);
#endif
	if(!NB_CMD_Send(cmd, strlen(cmd), timeout)){			  //写命令到网络设备
		if(usart2_buf.buf_count > 0) {
			NB_DEVICE_info.data_bytes = 0;
			memset(NB_DEVICE_info.resp, 0, sizeof(NB_DEVICE_info.resp));
			memcpy(NB_DEVICE_info.resp, usart2_buf.buf, usart2_buf.buf_count);
			NB_DEVICE_info.data_bytes = usart2_buf.buf_count;
			if(expect != NULL) {										//如果为空，则只是发送
				if(strstr(NB_DEVICE_info.resp, expect))	result = 0;
			} else {
				result = 0;
			}
		} else {
			NB_DEVICE_info.data_bytes = 0;
			memset(NB_DEVICE_info.resp, 0, sizeof(NB_DEVICE_info.resp));
		}
	}
#ifdef NB_DEBUG
	UsartPrintf(USART_DEBUG, "[DEBUG Send] NB Receive(%d):%s\r\n>>Expect:\"%s\", Result:%s\r\n", NB_DEVICE_info.data_bytes, NB_DEVICE_info.resp, expect, result?"Failed":"OK");
#endif
	NB_DEVICE_FreeBus(0);
	return result;
}

/*//==========================================================
//	函数名称：	NB_DEVICE_Receive
//
//	函数功能：	接收一条数据，并等待正确的响应
//
//	入口参数：	expect：期望内容
//				timeout(以系统时基为准)
//
//	返回参数：	返回连接结果
//
//	说明：		0-成功		1-失败
//==========================================================*/
_Bool NB_DEVICE_Receive(char *expect, unsigned short timeout){
	_Bool result = 1;
	if(!NB_IO_Read(timeout)){
		if(usart2_buf.buf_count > 0) {
			NB_DEVICE_info.data_bytes = 0;
			memset(NB_DEVICE_info.resp, 0, sizeof(NB_DEVICE_info.resp));
			memcpy(NB_DEVICE_info.resp, usart2_buf.buf, usart2_buf.buf_count);
			NB_DEVICE_info.data_bytes = usart2_buf.buf_count;
			if(expect != NULL) {
				if(strstr(NB_DEVICE_info.resp, expect))	result = 0;
			} else {
				result = 0;
			}
		} else {
			NB_DEVICE_info.data_bytes = 0;
			memset(NB_DEVICE_info.resp, 0, sizeof(NB_DEVICE_info.resp));
		}
#ifdef NB_DEBUG
		UsartPrintf(USART_DEBUG, "[DEBUG Rev] NB Receive(%d):%s\r\n>>Expect:\"%s\", Result:%s\r\n", NB_DEVICE_info.data_bytes, NB_DEVICE_info.resp, expect, result?"Failed":"OK");
#endif
	}
	return result;
}

/*//==========================================================
//	函数名称：	NB_DEVICE_SendData
//
//	函数功能：	使网络设备发送数据到平台
//
//	入口参数：	data：需要发送的数据
//				len：数据长度
//
//	返回参数：	0-发送完成	1-发送失败
//
//	说明：		
//==========================================================*/
_Bool NB_DEVICE_SendData(unsigned int msgId, unsigned int objectid, unsigned int instanceid, unsigned int resourceid, unsigned int valueType, unsigned int value, char *str){
	_Bool result = 1;
	unsigned char buffer_len = 128;
	char *buffer = (char *)NET_MallocBuffer(buffer_len);
	if(buffer == NULL) {
		UsartPrintf(USART_DEBUG, "[ERROR] 申请不到动态内存(NB_DEVICE_SendData)\r\n");
		NET_FreeBuffer(buffer);
		return 1;
	}
	if(NB_DEVICE_IsBusReady(1) == 1)
		return 1;
	NB_DEVICE_Delay(1);
	memset(buffer, 0, buffer_len);// AT+MIPLNOTIFY=0,5555,3316,0,5700,4,5,48.00,0,0 
	if( valueType == 1 ){
		sprintf(buffer, NET_AT_CMD_MIPLNOTIFY_S, msgId, objectid, instanceid, resourceid, valueType, strlen(str), str);	//发送命令
	} else if( valueType == 3 ) {
		sprintf(buffer, NET_AT_CMD_MIPLNOTIFY_I, msgId, objectid, instanceid, resourceid, valueType, value);
	} else if( valueType == 4 ) {
		sprintf(buffer, NET_AT_CMD_MIPLNOTIFY_F, msgId, objectid, instanceid, resourceid, valueType, value * 1.0);
	} else if( valueType == 5 ){
		sprintf(buffer, NET_AT_CMD_MIPLNOTIFY_B, msgId, objectid, instanceid, resourceid, valueType, value);	//发送命令
	}
#ifdef NB_DEBUG
	UsartPrintf(USART_DEBUG, "[DEBUG] NB SendData: %s\r\n", buffer);
#endif
	memset(NB_DEVICE_info.resp, 0, sizeof(NB_DEVICE_info.resp));
	NB_IO_Send(buffer, buffer_len);
	if(!NB_DEVICE_MIPLEvent(0, 26)){
		result = 0;
	} else if(strstr(NB_DEVICE_info.resp, "OK")) {
		result = 0;
	}
#ifdef NB_DEBUG
	if(result)
		UsartPrintf(USART_DEBUG, "[ERROR] 发送数据失败, Receive：%s\r\n", NB_DEVICE_info.resp);
	else
		UsartPrintf(USART_DEBUG, "[INFO] 发送数据成功, Receive：%s\r\n", NB_DEVICE_info.resp);
#endif
	NET_FreeBuffer(buffer);
	NB_DEVICE_Delay(10);
	NB_DEVICE_FreeBus(1);
	return result;
}








