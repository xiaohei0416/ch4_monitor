/**
	************************************************************
	************************************************************
	************************************************************
	*	文件名： 	net_io.c
	*
	*	说明： 		网络设备数据IO层
							RX:PA2
							TX:PA3
	*
	*	修改记录：	
	*	说明：	rb写入：在串口接收里边循环写入。
	*								rb读取：核心思想是在一帧完整数
	*										据尾添加结束符，上层应
	*										用根据结束符来获取完整
	*										的数据。
	************************************************************
	************************************************************
	************************************************************
**/

//单片机头文件
#include "stm32f10x.h"

//网络设备数据IO
#include "net_io.h"

//硬件驱动
#include "delay.h"
#include "usart.h"

#include "string.h"

const unsigned char tag = '\n';			//自定义结束符

/*
************************************************************
*	函数名称：	NET_IO_Init
*
*	函数功能：	初始化网络设备IO驱动层
*
*	入口参数：	无
*
*	返回参数：	无
*
*	说明：		底层的数据收发驱动
************************************************************
*/
void NB_IO_Init(void)
{

	Usart2_Init(9600);
#if(USART_DMA_RX_EN == 1)
	USARTx_ResetMemoryBaseAddr(NB_IO, (unsigned int)usart2_buf.buf, USART_BUFFER_SIZE, USART_RX_TYPE);
#endif

}

/*
************************************************************
*	函数名称：	NET_IO_Send
*
*	函数功能：	发送数据
*
*	入口参数：	str：需要发送的数据
*				len：数据长度
*
*	返回参数：	无
*
*	说明：		底层的数据发送驱动
*
************************************************************
*/
void NB_IO_Send(char *str, unsigned short len)
{
	Usart_SendString(NB_IO, (unsigned char *)str, len);
}

/*
************************************************************
*	函数名称：	NB_CMD_Send
*
*	函数功能：	发送指令并等待返回数据
*
*	入口参数：	cmd 指令内容
* 						len 指令长度
* 						timeout 超时时间
*
*	返回参数：	0：OK
				1：Timeout
*
*	说明：		
************************************************************
*/
unsigned char NB_CMD_Send(char *cmd, unsigned short len, unsigned short timeout)
{
	char result = 0;
	NB_IO_Send(cmd, len);
    if (timeout > 0)
        result = NB_IO_Read(timeout);
	return result;
}

/*
************************************************************
*	函数名称：	NB_IO_Read
*
*	函数功能：	读取接收的数据
*
*	入口参数：	timeout 超时时间
*
*	返回参数：	0：OK
* 						1：Timeout
*
*	说明：		
************************************************************
*/
unsigned char NB_IO_Read(unsigned short timeout)
{
	while(!usart2_buf.rev_idle && --timeout)		osDelay(1);
	if(timeout == 0) return 2;
	if(usart2_buf.rev_idle)
	{
		usart2_buf.rev_idle = 0;
		return 0;
	}
	return 1;
}

