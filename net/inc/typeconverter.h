#ifndef _TYPE_CONVERTER_
#define _TYPE_CONVERTER_

#include <stdlib.h>
#include <stdio.h>

void HEXSTR2STR(const char *instr, char *outstr, unsigned short len) {
    char CHAR[3];
    CHAR[2] = '\0';
    for(; len; len--) {
        CHAR[0] = (instr++)[0];
        CHAR[1] = (instr++)[0];
        (outstr++)[0] = strtol(CHAR, NULL, 16);
    }
}

void STR2HEXSTR(const char *instr, char *outstr, unsigned short len) {
    char CHAR[3];
    CHAR[2] = '\0';
    for(; len; len--) {
        sprintf(CHAR, "%02X", (instr++)[0]);
        (outstr++)[0] = CHAR[0];
        (outstr++)[0] = CHAR[1];
    }
    outstr[0] = '\0';
}



#endif
