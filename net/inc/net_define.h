#ifndef _NET_DEFINE_
#define _NET_DEFINE_


#define NET_RESET_IO_GROUP		GPIOA
#define NET_RESET_IO_PIN			GPIO_Pin_4//RE
#define NET_RESET_IO_NAME			"nb_reset"

#define NET_WAKEUP_IO_GROUP		GPIOA
#define NET_WAKEUP_IO_PIN			GPIO_Pin_5//PSM
#define NET_WAKEUP_IO_NAME		"nb_wakeup"

#define NET_AT_CMD_AT			    "AT\r\n"
#define NET_AT_CMD_ATE0			  "ATE0\r\n"
#define NET_AT_CMD_CPIN			  "AT+CPIN?\r\n"
#define NET_AT_CMD_CSQ			  "AT+CSQ\r\n"
#define NET_AT_CMD_CEREG			"AT+CEREG?\r\n"
#define NET_AT_CMD_CGPADDR		"AT+CGPADDR\r\n"

#define NET_AT_CMD_CPSMS			"AT+CPSMS=0\r\n"
#define NET_AT_CMD_CEDRXS     "AT+CEDRXS=1,5,\"0010\"\r\n"
#define NET_AT_CMD_QCCID      "AT+QCCID\r\n"
#define NET_AT_CMD_CIMI			  "AT+CIMI\r\n"
#define NET_AT_CMD_CGSN			  "AT+CGSN=1\r\n"
#define NET_AT_CMD_CCLK     	"AT+QCCLK?\r\n"
#define NET_AT_CMD_QNTP     	"AT+QNTP=1,\"ntp.aliyun.com\",123,1\r\n"

#define NET_AT_CMD_QPING     	"AT+QPING=1,\"%s\",1,1,32\r\n"

#define NET_AT_CMD_MIPLCREATE	    	"AT+MIPLCREATE\r\n"
#define NET_AT_CMD_MIPLDELETE	    	"AT+MIPLDELETE\r\n"
#define NET_AT_CMD_MIPLADDOBJ_CH	  "AT+MIPLADDOBJ=0,3300,1,\"1\",1,0\r\n" // ����
#define NET_AT_CMD_MIPLADDOBJ_TL2	  "AT+MIPLADDOBJ=0,3311,1,\"1\",2,0\r\n" // �¶ȡ�led2
#define NET_AT_CMD_MIPLADDOBJ_H	   	"AT+MIPLADDOBJ=0,3306,1,\"1\",1,1\r\n" // ʪ��
#define NET_AT_CMD_MIPLADDOBJ_L1	  "AT+MIPLADDOBJ=0,3201,1,\"1\",1,0\r\n" // led1
#define NET_AT_CMD_MIPLADDOBJ_L3	  "AT+MIPLADDOBJ=0,3312,1,\"1\",1,0\r\n" // led3
#define NET_AT_CMD_MIPLADDOBJ_FMQ	  "AT+MIPLADDOBJ=0,3338,1,\"1\",1,0\r\n" // ������
#define NET_AT_CMD_MIPLADDOBJ_FJ	  "AT+MIPLADDOBJ=0,3340,1,\"1\",1,0\r\n" // ���
#define NET_AT_CMD_MIPLOPEN	        "AT+MIPLOPEN=0,3600\r\n"
#define NET_AT_CMD_MIPLUPDATE       "AT+MIPLUPDATE=0,3600,0\r\n"
#define NET_AT_CMD_MIPLCLOSE	    	"AT+MIPLCLOSE=0\r\n"
#define NET_AT_CMD_MIPLOBSERVERSP		"AT+MIPLOBSERVERSP=%s,%s,%s\r\n"
#define NET_AT_CMD_MIPLDISCOVERRSP1 "AT+MIPLDISCOVERRSP=%s,%s,1,4,\"5700\"\r\n"  // ����//3300
#define NET_AT_CMD_MIPLDISCOVERRSP2 "AT+MIPLDISCOVERRSP=%s,%s,1,9,\"5850;5851\"\r\n" // �¶ȡ�led2//3311
#define NET_AT_CMD_MIPLDISCOVERRSP3 "AT+MIPLDISCOVERRSP=%s,%s,1,4,\"5851\"\r\n" // ʪ��//3306
#define NET_AT_CMD_MIPLDISCOVERRSP4 "AT+MIPLDISCOVERRSP=%s,%s,1,4,\"5550\"\r\n" // led1//3201
#define NET_AT_CMD_MIPLDISCOVERRSP5 "AT+MIPLDISCOVERRSP=%s,%s,1,4,\"5850\"\r\n" // led3;������;���//3312,3338,3340

// AT+MIPLNOTIFY=0,5555,3316,0,5700,4,5,48.00,0,0 
#define NET_AT_CMD_MIPLNOTIFY_F	    "AT+MIPLNOTIFY=0,%d,%d,%d,%d,%d,5,%.2f,0,0\r\n"  // float
#define NET_AT_CMD_MIPLNOTIFY_B	    "AT+MIPLNOTIFY=0,%d,%d,%d,%d,%d,1,%d,0,0\r\n"      // boolean
#define NET_AT_CMD_MIPLNOTIFY_S	    "AT+MIPLNOTIFY=0,%d,%d,%d,%d,%d,%d,%s,0,0\r\n"     // string
#define NET_AT_CMD_MIPLNOTIFY_I	    "AT+MIPLNOTIFY=0,%d,%d,%d,%d,%d,4,%d,0,0\r\n"      // integer


#endif
