#ifndef _NB_DEVICE_H_
#define _NB_DEVICE_H_

#define NB_DEBUG


//=============================配置==============================
//===========可以提供RTOS的内存管理方案，也可以使用C库的=========
#define NET_MallocBuffer	malloc

#define NET_FreeBuffer		free
//==========================================================


#define NET_TIME_EN				1	//1-获取网络时间		0-不获取


struct _MSG_ID
{
    unsigned int ch4;
    unsigned int templed2;
    unsigned int humi;
    unsigned int led1;
    unsigned int led3;
    unsigned int fmq;
		unsigned int fj;
};

typedef struct
{
	char resp[128];			//数据缓存
	
	int data_bytes;					//接收到的数据长度
		
	unsigned int net_time;			//网络时间
	
	char imei[16]; 					// IMEI 获取 模块 识别码
	char imsi[16];					// IMSI 获取 SIM 卡标识
	
	unsigned int ref;		        //基础通信套件的一个标识
	struct _MSG_ID msgId; 		// MIPLNOTIFY msgid
	
	unsigned char err : 4; 			//错误类型
	unsigned char init_step : 4;	//初始化步骤
	unsigned char conn_step : 4;	//连接平台步骤
	unsigned char reboot : 1;		//死机重启标志
	unsigned char net_work : 1;	    //网络访问OK
	unsigned char onenet : 1;	    //平台访问OK
	unsigned char send_failed_count : 4;	//数据发送失败计数
	unsigned char reverse : 2;		//保留

} NB_DEVICE_INFO;

extern NB_DEVICE_INFO NB_DEVICE_info;


typedef struct
{
    unsigned int init_delay;    //初始化延时
    unsigned char uart_test;    //串口检测次数
    unsigned char pin_test;     //SIM卡状态检测次数
    unsigned char signal_test;  //信号强度检测次数
    unsigned char net_test;     //NB网络强度检测次数    
} NB_DEVICE_INIT;


#define NB_DEVICE_NO_NETWORK	    0	//无网络
#define NB_DEVICE_CONNECT			1	//已接入NB网络
#define NB_DEVICE_GOT_IP			2	//已获取到IP， PPP链接
#define NB_DEVICE_CLOSED			3	//已断开PPP链接
#define NB_DEVICE_NO_DEVICE		    4	//无设备
#define NB_DEVICE_INITIAL			5	//初始化状态
#define NB_DEVICE_NO_CARD			6	//没有sim卡
#define NB_DEVICE_BUSY				254	//忙
#define NB_DEVICE_NO_ERR			255 //无错误

void NB_DEVICE_IO_Init(void);

_Bool NB_DEVICE_Reset(void);

_Bool NB_DEVICE_GetIMEI(void);

_Bool NB_DEVICE_GetIMSI(void);

_Bool NB_DEVICE_Init(void);

_Bool NB_DEVICE_Close(void);

_Bool NB_DEVICE_Connect(void);

_Bool NB_DEVICE_Update(void);

_Bool NB_DEVICE_MIPLEvent(unsigned char ref, unsigned char event);

_Bool NB_DEVICE_SendCmd(char *cmd, char *expect, unsigned short timeout); 

_Bool NB_DEVICE_Receive(char *expect, unsigned short timeout);

_Bool NB_DEVICE_SendData(unsigned int msgId, unsigned int objectid, unsigned int instanceid, unsigned int resourceid, unsigned int valueType, unsigned int value, char *str);


#endif
