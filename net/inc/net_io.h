#ifndef _NET_IO_H_
#define _NET_IO_H_

#define NB_IO		USART2


void NB_IO_Init(void);

void NB_IO_Send(char *str, unsigned short len);

unsigned char NB_CMD_Send(char *cmd, unsigned short len, unsigned short timeout);

unsigned char NB_IO_Read(unsigned short timeout);


#endif
