#ifndef _DELAY_H_
#define _DELAY_H_

#include "cmsis_os2.h"



#define RTOS_TimeDly(millisec) 						osDelay(millisec)

#define RTOS_EnterInt()								osKernelLock()
#define RTOS_ExitInt()								osKernelUnlock()

#define RTOS_ENTER_CRITICAL()						osKernelLock()
#define RTOS_EXIT_CRITICAL()						osKernelUnlock()




void Delay_Init(void);

void DelayUs(unsigned short us);

void DelayXms(unsigned short ms);

#endif
