#ifndef _STMFLASH_H_
#define _STMFLASH_H_



unsigned int FLASH_GetStartAddr(unsigned char page, _Bool flash_type);

void FLASH_Erase(unsigned char page, unsigned char num, _Bool flash_type);

void Flash_Read(unsigned char page, unsigned short *rBuf, unsigned short len, _Bool flash_type);

unsigned short FLASH_ReadHalfWord(unsigned char page, unsigned short offset, _Bool flash_type);

unsigned int FLASH_ReadWord(unsigned char page, unsigned short offset, _Bool flash_type);

void Flash_Write(unsigned char page, unsigned short *wBuf, unsigned short len, _Bool flash_type);

unsigned int FLASH_ReadWord(unsigned char page, unsigned short offset, _Bool flash_type);


#endif
