
/*
 * Auto generated Run-Time-Environment Configuration File
 *      *** Do not modify ! ***
 *
 * Project: 'bishe' 
 * Target:  'Target 1' 
 */

#ifndef RTE_COMPONENTS_H
#define RTE_COMPONENTS_H


/*
 * Define the Device Header File: 
 */
#define CMSIS_device_header "stm32f10x.h"

/*  ARM::CMSIS:RTOS2:Keil RTX5:Library:5.5.2 */
#define RTE_CMSIS_RTOS2                 /* CMSIS-RTOS2 */
        #define RTE_CMSIS_RTOS2_RTX5            /* CMSIS-RTOS2 Keil RTX5 */
/*  Keil::Device:StdPeriph Drivers:ADC:3.5.0 */
#define RTE_DEVICE_STDPERIPH_ADC
/*  Keil::Device:StdPeriph Drivers:DMA:3.5.0 */
#define RTE_DEVICE_STDPERIPH_DMA
/*  Keil::Device:StdPeriph Drivers:Framework:3.5.1 */
#define RTE_DEVICE_STDPERIPH_FRAMEWORK
/*  Keil::Device:StdPeriph Drivers:GPIO:3.5.0 */
#define RTE_DEVICE_STDPERIPH_GPIO
/*  Keil::Device:StdPeriph Drivers:I2C:3.5.0 */
#define RTE_DEVICE_STDPERIPH_I2C
/*  Keil::Device:StdPeriph Drivers:IWDG:3.5.0 */
#define RTE_DEVICE_STDPERIPH_IWDG
/*  Keil::Device:StdPeriph Drivers:RCC:3.5.0 */
#define RTE_DEVICE_STDPERIPH_RCC
/*  Keil::Device:StdPeriph Drivers:TIM:3.5.0 */
#define RTE_DEVICE_STDPERIPH_TIM
/*  Keil::Device:StdPeriph Drivers:USART:3.5.0 */
#define RTE_DEVICE_STDPERIPH_USART


#endif /* RTE_COMPONENTS_H */
