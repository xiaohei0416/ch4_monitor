#include "mq4.h"
#include "adc.h"
#include "mcu_gpio.h"
#include "led.h"
#include "relay.h"

MQ_4 mq_4;
void mq4_init(void){
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA|RCC_APB2Periph_ADC1|RCC_APB2Periph_AFIO,ENABLE);
	MCU_GPIO_Init(GPIOA, GPIO_Pin_1, GPIO_Mode_AIN, GPIO_Speed_50MHz, "temp");
	ADCx_Init(ADC1, 0);
}

void mq4_state(int i){
	if(i == 1){//����
		LED_Ctrl(1, LED_OFF);
		LED_Ctrl(2, LED_OFF);
		LED_Ctrl(0, LED_ON);
		RELAY_Ctrl(0, RELAY_OFF);
		RELAY_Ctrl(1, RELAY_OFF);
	} else if(i == 2){//Σ
		LED_Ctrl(0, LED_OFF);
		LED_Ctrl(2, LED_OFF);
		LED_Ctrl(1, LED_ON);
		RELAY_Ctrl(0, RELAY_OFF);
		RELAY_Ctrl(1, RELAY_ON);
	} else if(i == 3){//����
		LED_Ctrl(1, LED_OFF);
		LED_Ctrl(0, LED_OFF);
		LED_Ctrl(2, LED_ON);
		RELAY_Ctrl(0, RELAY_ON);
		RELAY_Ctrl(1, RELAY_ON);
	}
}

