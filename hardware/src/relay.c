//单片机头文件
#include "stm32f10x.h"

//单片机相关组件
#include "mcu_gpio.h"

#include "relay.h"
#include "delay.h"


const static GPIO_LIST relay_gpio_list[RELAY_NUM] = {
													{GPIOA, GPIO_Pin_7,  "fmq"},
													{GPIOA, GPIO_Pin_6,  "fengshan"}
												};

RELAY_STATUS relay_status;

/*
************************************************************
*	函数名称：	RELAY_Init
*
*	函数功能：	RELAY 初始化
*
*	入口参数：	无
*
*	返回参数：	无
*
*	说明：		高电平吸合		低电平断开
************************************************************
*/
void RELAY_Init(void) {
	for(int i=0; i<=RELAY_NUM; i++) {
		MCU_GPIO_Init(relay_gpio_list[i].gpio_group, relay_gpio_list[i].gpio_pin, GPIO_Mode_Out_PP, GPIO_Speed_50MHz, relay_gpio_list[i].gpio_name);
		RTOS_TimeDly(1);
		RELAY_Ctrl(i, RELAY_OFF);
	}
}

/*
************************************************************
*	函数名称：	RELAY_Ctrl
*
*	函数功能：	继电器输出控制
*
*	入口参数：	status：开关状态
*
*	返回参数：	无
*
*	说明：		
************************************************************
*/
void RELAY_Ctrl(unsigned char num, RELAY_ENUM status){
	RTOS_ENTER_CRITICAL();
	MCU_GPIO_Output_Ctl(relay_gpio_list[num].gpio_name, status);
	relay_status.status[num] = status;
	RTOS_EXIT_CRITICAL();
}


/*
************************************************************
*	函数名称：	RELAY_Read
*
*	函数功能：	读取继电器状态
*
*	入口参数：	无
*
*	返回参数：	开关状态
*
*	说明：		
************************************************************
*/
_Bool RELAY_Read(unsigned char num){
	RTOS_ENTER_CRITICAL();
	relay_status.status[num] = MCU_GPIO_Input_Read(relay_gpio_list[num].gpio_name);
	RTOS_EXIT_CRITICAL();
	return relay_status.status[num];
}
