#ifndef _RELAY_H_
#define _RELAY_H_

#define RELAY_NUM			2

typedef enum{
	RELAY_OFF = 0,
	RELAY_ON
} RELAY_ENUM;

typedef struct{
	_Bool status[RELAY_NUM];
	_Bool status_temp[RELAY_NUM];
} RELAY_STATUS;

extern RELAY_STATUS relay_status;

void RELAY_Init(void);

void RELAY_Ctrl(unsigned char num, RELAY_ENUM status);

_Bool RELAY_Read(unsigned char num);

#endif
