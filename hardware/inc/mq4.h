#ifndef _MQ4_H_
#define _MQ4_H_

#define MQ4 "mq4"

typedef struct{
	unsigned int mq;
} MQ_4;

extern MQ_4 mq_4;

void mq4_init(void);

void mq4_state(int i);

#endif
