#include "cmsis_os2.h"
#include "thread.h"
#include "usart.h"
#include "delay.h"
#include "adc.h"
#include "i2c.h"
#include "iwdg.h"

#include "net_device.h"
#include "dht11.h"
#include "mq4.h"
#include "led.h"
#include "relay.h"
#include "oled.h"

//C库
#include <string.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>

unsigned int watchdog = 0;

void Init_Hardware(void){			// 硬件初始化
	Usart1_Init(115200);				// 串口1初始化为调试串口（打印）
	UsartPrintf(USART_DEBUG, "\r\n====================================\r\n");
#if(USART_DMA_RX_EN)
	USARTx_ResetMemoryBaseAddr(USART_DEBUG, (unsigned int)usart1_buf.buf, sizeof(usart1_buf.buf), USART_RX_TYPE);
#endif
	UsartPrintf(USART_DEBUG, "THIS IS USART_DEBUG.\n");
  Delay_Init();								// 延时初始化
	NB_DEVICE_IO_Init();				// NB模块IO口初始化
	
	LED_Init();									// LED初始化
	RELAY_Init();								// 继电器初始化（GPIO口初始化）
	if(IIC_Init(OLED_IO)){			// IIC初始化
		UsartPrintf(USART_DEBUG, "OLED IIC 初始化失败\r\n");
	}
	DelayXms(1000);
  if(OLED_Exist()){
		OLED_Init();							// OLED初始化
  } else {
		UsartPrintf(USART_DEBUG, "OLED 不存在\r\n");
	}
	DelayXms(1000);
	mq4_init();									// MQ-4模块初始化
	DelayXms(1000);
	while(DHT11_Init()){				// DHT11初始化
		UsartPrintf(USART_DEBUG, "DHT11 Error \r\n");
		DelayXms(1000);
	}
	if(RCC_GetFlagStatus(RCC_FLAG_IWDGRST) == SET){ 								//如果是看门狗复位则提示
		UsartPrintf(USART_DEBUG, "\r\n[WARNING] IWDG Reboot !!!!!\r\n");
		RCC_ClearFlag();														//清除看门狗复位标志位
	}
	Iwdg_Init(4, 1250); 				// 看门狗初始化，64分频，每秒625次，重载1250次，2s
	
}
 
/*      线程任务			*/
void mq4_Task (void *argument);                   // 线程函数（下同
osThreadId_t tid_mq4_Task = NULL;                 // 线程 id （下同

void dht11_Task (void *argument);
osThreadId_t tid_dht11_Task = NULL;

void OLED_Task (void *argument);
osThreadId_t tid_OLED_Task = NULL;

void IWDG_Task (void *argument);
osThreadId_t tid_IWDG_Task;

void NB_Net_Task(void *argument);
osThreadId_t tid_NB_Net_Task = NULL;

void NB_Monitor_Task(void *argument);
osThreadId_t tid_NB_Monitor_Task = NULL;

void NB_Platform_Task(void *argument);
osThreadId_t tid_NB_Platform_Task = NULL;

void NB_Send_Task(void *argument);
osThreadId_t tid_NB_Send_Task = NULL;

/* 创建线程堆栈 */
const osThreadAttr_t Task_Attr = {
  .stack_size = 1024                            // 创建大小为1024字节的线程堆栈
};

const osThreadAttr_t NB_Task_Attr = {
  .stack_size = 1024
};

const osThreadAttr_t OLED_Attr = {
  .stack_size = 512
};

/*	函数名称：	Init_Thread
*
*	函数功能：	线程初始化
*
*	入口参数：	无
*
*	返回参数：	无
*
*	说明：			启动线程任务
************************************************************
*/
int Init_Thread (void) {
	int result = 0;
	int err_count = 0;
	
	err_count++;
	tid_IWDG_Task = osThreadNew(IWDG_Task, NULL, &Task_Attr);
	if (tid_IWDG_Task == NULL) {
		result = err_count;
	}
	
	err_count++;
	tid_NB_Monitor_Task = osThreadNew(NB_Monitor_Task, NULL, &NB_Task_Attr);
	if (tid_NB_Monitor_Task == NULL) {
		result = err_count;
	}
	
//	err_count++;
//	tid_mq4_Task = osThreadNew(mq4_Task, NULL, &NB_Task_Attr);
//	if (tid_mq4_Task == NULL) {
//		result = err_count;
//	}
	
  return result;
}

/*	函数名称：	Sys_Soft_Reset
*
*	函数功能：	软件重启stm32
*
*	入口参数：	无
*
*	返回参数：	无
*
*	说明：
************************************************************
*/
void Sys_Soft_Reset(void){
	SCB->AIRCR =0X05FA0000|(u32)0x04;
}

/*	函数名称：	IWDG_Task
*
*	函数功能：	清除看门狗
*
*	入口参数：	void类型的参数指针
*
*	返回参数：	无
*
*	说明：			看门狗任务
************************************************************
*/
void IWDG_Task(void *argument){
	while(1){
		if( watchdog++ < 7200U ) {
			Iwdg_Feed(); 		//喂狗
		}
		osDelay(500); 	//挂起任务500ms
	}
}

/*	函数名称：	NB_Monitor_Task
*
*	函数功能：	NB模块初始化、联网及其他任务调度执行
*
*	入口参数：	void类型的参数指针
*
*	返回参数：	无
*
*	说明：			
************************************************************
*/
void NB_Monitor_Task(void *argument) {
	osStatus_t result;
	while(1) {
		if(!NB_DEVICE_info.net_work) {
			if(tid_NB_Net_Task == NULL) {
				tid_NB_Net_Task = osThreadNew(NB_Net_Task, NULL, &NB_Task_Attr);
				if(tid_NB_Net_Task == NULL)
					UsartPrintf(USART_DEBUG, "[NB] 创建NB模块初始化线程失败，Error\r\n");
			}
    } else if(tid_NB_Net_Task) {
			result = osThreadTerminate(tid_NB_Net_Task);
			if(result == osOK) {
				UsartPrintf(USART_DEBUG, "[NB] 连接网络成功，结束线程\r\n");
				tid_NB_Net_Task = NULL;
			}
		} else if(!NB_DEVICE_info.onenet) {
			if(tid_NB_Platform_Task == NULL) {
				tid_NB_Platform_Task = osThreadNew(NB_Platform_Task, NULL, &NB_Task_Attr);
				if(tid_NB_Platform_Task == NULL)
					UsartPrintf(USART_DEBUG, "[NB] 创建OneNET初始化线程失败，Error\r\n");
			}
		} else if(tid_NB_Platform_Task) {
			result = osThreadTerminate(tid_NB_Platform_Task);
			if(result == osOK) {
				UsartPrintf(USART_DEBUG, "[NB] 连接平台成功，结束线程\r\n");
				tid_NB_Platform_Task = NULL;
			}
		} else if(tid_NB_Send_Task == NULL) {
			tid_NB_Send_Task = osThreadNew(NB_Send_Task, NULL, &NB_Task_Attr);
			if(tid_NB_Send_Task)
				UsartPrintf(USART_DEBUG, "[NB] 启动数据发送线程\r\n");
			else
				UsartPrintf(USART_DEBUG, "<ERROR>[NB] 数据发送线程启动失败\r\n");
		} else if(tid_mq4_Task == NULL) {
			tid_mq4_Task = osThreadNew(mq4_Task, NULL, &Task_Attr);
			if(tid_mq4_Task)
				UsartPrintf(USART_DEBUG, "[NB] 启动甲烷监测线程\r\n");
			else
				UsartPrintf(USART_DEBUG, "<ERROR>[NB] 甲烷监测线程启动失败\r\n");
		} else if(tid_OLED_Task == NULL) {
			tid_OLED_Task = osThreadNew(OLED_Task, NULL, &OLED_Attr);
			if(tid_OLED_Task)
				UsartPrintf(USART_DEBUG, "[NB] 启动OLED显示线程\r\n");
			else
				UsartPrintf(USART_DEBUG, "<ERROR>[NB] OLED显示线程启动失败\r\n");
		}
		if(NB_DEVICE_info.reboot != 0) {
			NB_DEVICE_info.net_work = 0;
			NB_DEVICE_info.init_step = 0;
			NB_DEVICE_info.onenet = 0;
			NB_DEVICE_info.conn_step = 0;
			NB_DEVICE_info.send_failed_count = 0;
			result = osThreadTerminate(tid_IWDG_Task);		//杀狗，重启
			if(result == osOK) {
				UsartPrintf(USART_DEBUG, "[NB] 数据发送超时，系统重启!!!\r\n");
				NB_DEVICE_info.reboot = 0;
			} else {
				UsartPrintf(USART_DEBUG, "[NB] 数据发送超时，请重启系统!!!\r\n");
			}
		}
		osDelay(1000);
	}
}

void NB_Net_Task(void *argument) {
	_Bool net_status = 1;
	while(1) {
		if(net_status)
			net_status = NB_DEVICE_Init();
		osDelay(1000);
	}
}

void NB_Platform_Task(void *argument) {
	_Bool reg_status = 1;
	while(1) {
		if(reg_status)
			reg_status = NB_DEVICE_Connect();
		osDelay(1000);
	}
}

void NB_Send_Task(void *argument) {
//	uint32_t tick;
	unsigned int value;
	unsigned short send_data_count = 0;
	unsigned short send_failed_limit = 10;
	unsigned int   update_interval = 600; // Onenet更新间隔时间，实际时间将大于设定值
	unsigned short update_count = 0;
	unsigned short update_retry = 3;
	unsigned short update_retry_count = 0;
	led_status.led_temp[0] = -1;led_status.led_temp[1] = -1;led_status.led_temp[2] = -1;	// 启动时发送一次数据
	relay_status.status_temp[0] = -1;relay_status.status_temp[1] = -1;										// 启动时发送一次数据
	while(1) {
//		tick = osKernelGetTickCount();        // 获取系统嘀嗒数
			switch(send_data_count++) {
				case 0://甲烷
					value = mq_4.mq;
					if(value != 0){
						if(NB_DEVICE_SendData(NB_DEVICE_info.msgId.ch4, 3300, 0, 5700, 4, value, NULL)) 
							NB_DEVICE_info.send_failed_count++;
					} else {
						NB_DEVICE_info.send_failed_count++;
					}
					break;
				case 1://温度
					if(tehu.temp_temp != tehu.temp){
						tehu.temp_temp = tehu.temp;
						if(NB_DEVICE_SendData(NB_DEVICE_info.msgId.templed2, 3311, 0, 5851, 3, tehu.temp_temp, NULL)) 
							NB_DEVICE_info.send_failed_count++;
						break;
					}
					break;
				case 2://湿度
					if(tehu.humi_temp != tehu.humi){
						tehu.humi_temp = tehu.humi;
						if(NB_DEVICE_SendData(NB_DEVICE_info.msgId.humi, 3306, 0, 5851, 3, tehu.humi_temp, NULL)) 
							NB_DEVICE_info.send_failed_count++;
						break;
					}
					break;
				case 3://led1(green)
					if(led_status.led_temp[0] != led_status.led_status[0]){
						led_status.led_temp[0] = led_status.led_status[0];
						if(NB_DEVICE_SendData(NB_DEVICE_info.msgId.led1, 3201, 0, 5550, 5, led_status.led_temp[0], NULL)) 
							NB_DEVICE_info.send_failed_count++;
						break;
					}
					break;
				case 4://led2(yellow)
					if(led_status.led_temp[1] != led_status.led_status[1]){
						led_status.led_temp[1] = led_status.led_status[1];
						if(NB_DEVICE_SendData(NB_DEVICE_info.msgId.templed2, 3311, 0, 5850, 5, led_status.led_temp[1], NULL))
							NB_DEVICE_info.send_failed_count++;
						break;
					}
					break;
				case 5://led3(red)
					if(led_status.led_temp[2] != led_status.led_status[2]){
						led_status.led_temp[2] = led_status.led_status[2];
						if(NB_DEVICE_SendData(NB_DEVICE_info.msgId.led3, 3312, 0, 5850, 5, led_status.led_temp[2], NULL))
							NB_DEVICE_info.send_failed_count++;
						break;
					}
					break;
				case 6://蜂鸣器
					if(relay_status.status_temp[0] != relay_status.status[0]){
						relay_status.status_temp[0] = relay_status.status[0];
						if(NB_DEVICE_SendData(NB_DEVICE_info.msgId.fmq, 3338, 0, 5850, 5, relay_status.status_temp[0], NULL))
							NB_DEVICE_info.send_failed_count++;
						break;
					}
					break;
				case 7://风机
					if(relay_status.status_temp[1] != relay_status.status[1]){
						relay_status.status_temp[1] = relay_status.status[1];
						if(NB_DEVICE_SendData(NB_DEVICE_info.msgId.fj, 3340, 0, 5850, 5, relay_status.status_temp[1], NULL))
							NB_DEVICE_info.send_failed_count++;
						break;
					}
					break;
				default:
					send_data_count = 0;
					UsartPrintf(USART_DEBUG, "[NB_Send_Task] send_failed_count: %d\r\n", NB_DEVICE_info.send_failed_count);
					if(NB_DEVICE_info.send_failed_count >= send_failed_limit)
						NB_DEVICE_info.reboot = 1;
					else
						watchdog = 0;
					NB_DEVICE_info.send_failed_count = 0;
					osDelay(500);
			}
		if(tid_OLED_Task) {
			osThreadFlagsSet(tid_OLED_Task, 0x00000001U);
			osThreadFlagsWait(0x00000001U, osFlagsWaitAny, 5000U); // wait timeout 5s
		}
		if(update_count++ >= update_interval) {
			for(update_retry_count = 0; update_retry_count < update_retry; update_retry_count++) {
				if(!NB_DEVICE_Update()) {
					UsartPrintf(USART_DEBUG, "[NB_Update] 更新注册信息 成功\r\n");
					update_count = 0;
					break;
				} else {
					UsartPrintf(USART_DEBUG, "[NB_Update] 更新注册信息 失败\r\n");
				}
				osDelay(1000);
			}
			if(update_retry_count >= update_retry) {
				NB_DEVICE_info.reboot = 1;
			}
		}
		osDelay(500);
	}
}

void mq4_Task (void *argument) {
  while (1) {
//		sprintf(mq, "%.2f", ADCx_GetVoltag(ADC1, 1, 3)*100);
//		mq_4.mq = atoi(mq);
		if(ADCx_GetVoltag(ADC1, 1, 3)>=1.9){
			mq_4.mq = ADCx_GetVoltag(ADC1, 1, 3) * 2000 - 3500;
		} else {
			mq_4.mq = 300;
		}
		osDelay(1000);
		RTOS_ENTER_CRITICAL();
		if(DHT11_Read_Data(&tehu.temp,&tehu.humi)){
			UsartPrintf(USART_DEBUG, "temp error\r\n");
		}
		RTOS_EXIT_CRITICAL();
		if(mq_4.mq < 1000){																	//小于1000ppm
			mq4_state(1);
			UsartPrintf(USART_DEBUG, "[GREEN] 燃气值：%d\r\n", mq_4.mq);
			UsartPrintf(USART_DEBUG, "temp:%d℃,humi:%d%%\r\n",tehu.temp,tehu.humi);
		} else if(mq_4.mq >= 1000 && mq_4.mq < 1600){					//大于1000ppm小于1600ppm
			mq4_state(2);
			UsartPrintf(USART_DEBUG, "[YELLOW] 燃气值：%d\r\n", mq_4.mq);
			UsartPrintf(USART_DEBUG, "temp:%d℃,humi:%d%%\r\n",tehu.temp,tehu.humi);
		} else if(mq_4.mq >= 1600){													//大于1600ppm
			mq4_state(3);
			UsartPrintf(USART_DEBUG, "[RED] 燃气值：%d\r\n", mq_4.mq);
			UsartPrintf(USART_DEBUG, "temp:%d℃,humi:%d%%\r\n",tehu.temp,tehu.humi);
		}
		osDelay(1000);
  }
}

void OLED_Task(void *argument){
	char mq4[12];  
	char t[4], h[4];
	OLED_ClearScreen();		//清屏
	while (1) {
		osThreadFlagsWait(0x00000001U, osFlagsWaitAny, osWaitForever);
		if(OLED_ClearScreen()){		//清屏		
			UsartPrintf(USART_DEBUG, "clean err\r\n");
		}
		sprintf( mq4, "%d ppm", mq_4.mq );//赋值甲烷值
		sprintf( t, "%d", tehu.temp );//赋值温度值
		sprintf( h, "%d", tehu.humi );//赋值湿度值
		OLED_DisString8x16(0,  50, "CH4");
		OLED_DisString8x16(2,  36, mq4);//显示甲烷值
		OLED_ClearAt(4);  OLED_ClearAt(6);
		OLED_DisString6x8(4,  20, "Temp");
		OLED_DisString6x8(4,  60, "Humi");
		OLED_DisString6x8(6,  22, t);//显示温度值
		OLED_DisString6x8(6,  62, h);//显示湿度值
//		osDelay(1000);
		osThreadFlagsSet(tid_NB_Send_Task, 0x00000001U);
	}
}

