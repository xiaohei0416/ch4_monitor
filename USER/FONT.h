#ifndef _FONT_H_
#define _FONT_H_


extern unsigned char asc2_1206[95][12];
extern const unsigned char asc2_1608[95][16];
extern const unsigned char asc2_2412[95][36];

extern const unsigned char F6x8[][6];
extern const unsigned char F8X16[];


#endif
