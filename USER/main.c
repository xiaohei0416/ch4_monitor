/*----------------------------------------------------------------------------
 * 
 *---------------------------------------------------------------------------*/
 
#include "RTE_Components.h"
#include  CMSIS_device_header
#include "cmsis_os2.h"
#include "thread.h"
#include "usart.h"
 
/*----------------------------------------------------------------------------
 * 应用程序主线程
 *---------------------------------------------------------------------------*/

int main (void) {
  SystemCoreClockUpdate();						// 系统初始化
	Init_Hardware();										// 硬件初始化
  osKernelInitialize();               // CMSIS-RTOS初始化
	int thread_result;
	thread_result = Init_Thread();
	if (thread_result > 0)
		UsartPrintf(USART_DEBUG, "Thread init failed, code = %d\n", thread_result);
  osKernelStart();                    // 开始执行线程
  for (;;) {}
}
